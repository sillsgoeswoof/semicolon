CONSTANTS = {  # meta bot data that generally shouldn't be translated or changed unless hosting your own bot
    "cog_folder": "cogs",
    "data_folder": "data",
    "lang_folder": "langs",
    "chip": 381941901462470658,
    "max_pokemon": 893,
    "pkmn_sprites_dir": "pkmn_sprites",
    "emojis": {  # from Bot-Moji: https://discord.gg/SyKBWjP
        "SUCCESS": ["<:greenTick:328630479886614529>", "\N{WHITE HEAVY CHECK MARK}"],
        "FAILURE": ["<:redTick:328630479576104963>", "\N{CROSS MARK}"],
        "JOIN": ["<:enter:465369329836228618>", "\N{BLACK RIGHTWARDS ARROW}"],
        "LEAVE": ["<:quit:465369329324523542>", "\N{AIRPLANE DEPARTURE}"],
        "BAN": ["<:banned:683597796955258906>", "\N{HAMMER}"],
        "UNBAN": ["<:unbanned:683597796980424734>", "\N{OPEN LOCK}"],
        "RATELIMIT": ["<:refresh:465369330323030016>", "\N{CLOCK FACE THREE OCLOCK}"],
        "NEW_JOIN": "\N{CLOCK FACE THREE OCLOCK} ",
        "DM_SENT": "\N{OPEN MAILBOX WITH RAISED FLAG}",
        "REPUBLICAN": "\N{LARGE RED CIRCLE}",
        "DEMOCRAT": "\N{LARGE BLUE CIRCLE}",
        "EVERYBOARD_UNUSABLE": "[?]",
        "UNKNOWN": "\N{BLACK QUESTION MARK ORNAMENT}"
    },
    "colors": {
        "default": 0x88297c,
        "message_edit": 0xf5ba5b,
        "message_delete": 0xde3d28
    },
    "everyboard_extensions": ["jpg", "png", "gif", "jpeg"],  # embeddable file types
    "pronoun_whitelist": ["she/her", "he/him", "they/them", "any pronouns", "no pronouns"],  # default/global roles
    "reddit_sort": ["hot", "new", "rising", "top", "controversial"],
    "reddit_timeable": ["top", "controversial"],
    "reddit_time": ["hour", "day", "week", "month", "year", "all"],
    "self_delete": 10,  # how long to wait before deleting approval messages
    "clear_count_warning_threshold": 100,  # highest number of messages that can be cleared before asking for confirmation
    "source_code": "https://gitlab.com/lexikiq/semicolon",
    "source_code_branch": "https://gitlab.com/lexikiq/semicolon/tree/python-ext.com",
    "game": "{}help for help!",
    "description": '''Collection of various tools for entertainment, moderation, and more.
                      Created by lexikiq#0493 (140564059417346049)'''
}
