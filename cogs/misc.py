import inspect
import re
from typing import List

from cogs.utils.utils import *
from cogs.utils.vars import CONSTANTS
import datetime
import humanize
import random


class Miscellaneous(commands.Cog):
    def __init__(self, bot):
        self.bot = bot

    @commands.command(aliases=['pong', 'pog', 'pingpong'])
    @commands.cooldown(rate=1, per=3, type=commands.BucketType.channel)
    async def ping(self, ctx: commands.Context):
        """Is the bot alive? If you're reading this, probably."""
        unit = 'microseconds'  # can also be 'milliseconds', 'seconds'

        cmdstr = ctx.invoked_with
        cmdstr = cmdstr[0].upper() + cmdstr[1:] + '!'
        output = [cmdstr]
        msg = await ctx.send(output[0])

        delay = msg.created_at - ctx.message.created_at
        output.append(self.bot.i18n.localize('ping1', ctx=ctx).format(humanize.naturaldelta(delay, minimum_unit=unit)))
        ltncy = datetime.timedelta(seconds=ctx.bot.latency)
        output.append(self.bot.i18n.localize('ping2', ctx=ctx).format(humanize.naturaldelta(ltncy, minimum_unit=unit)))

        await msg.edit(content='\n'.join(output))

    @commands.command()
    @commands.is_owner()
    async def echo(self, ctx: commands.Context, *, text: str):
        await ctx.send(text)

    @commands.command()
    @commands.cooldown(rate=1, per=5, type=commands.BucketType.channel)
    async def reddit(self, ctx: commands.Context, subreddit: str, limit: int = 100, sort: str = "hot", time: str = "day"):
        """Grabs a recent image from the specified subreddit.
        'limit' is an integer upto 100 (the default) that sets the limit for the number of posts to get and pick from.
        'sort' can be one of 'hot', 'new', 'rising', 'top', 'controversial'
        'time' is used with 'top' and 'controversial' and can be one of 'hour', 'day', 'week', 'month', 'year', 'all'"""
        async with ctx.typing():
            limit = max(1, min(100, limit))
            sort = sort.lower()
            time = time.lower()
            # input validation
            if sort not in CONSTANTS['reddit_sort']:
                await ctx.send(self.bot.i18n.localize('invalid_argument', ctx=ctx).format('sort', '`, `'.join(CONSTANTS['reddit_sort'])))
                return
            url = 'https://www.reddit.com/r/{0}/{1}/.json?count={2}&limit={2}'.format(subreddit, sort, limit)
            if sort in CONSTANTS['reddit_timeable']:
                if time not in CONSTANTS['reddit_time']:
                    await ctx.send(self.bot.i18n.localize('invalid_argument', ctx=ctx).format('time', '`, `'.join(CONSTANTS['reddit_time'])))
                    return
                url += f'&t={time}'

            async with ctx.bot.session.get(url, headers={"User-Agent": "semicolon-bot:v1.1 (/u/noellekiq)"}) as r:
                if r.status != 200:
                    error = error_gen(ctx,
                                      self.bot.i18n.localize('connection_failure', ctx=ctx).format('reddit', r.status,
                                                                                                   await r.text()))
                    await ctx.send(embed=error)
                    return
                jsondata = await r.json()

            if 'error' in jsondata:
                await ctx.send(embed=error_gen(ctx, self.bot.i18n.localize('reddit_data_failed', ctx=ctx)))
                return

            posts = jsondata['data']['children']
            if isinstance(ctx.channel, discord.TextChannel):
                includensfw = ctx.channel.is_nsfw()
            else:
                includensfw = True

            viableposts = []
            for post in posts:
                pdata = post['data']
                is_safe = not pdata['over_18'] or (pdata['over_18'] and includensfw)
                if "post_hint" in pdata and pdata['post_hint'] == 'image' and is_safe:
                    viableposts.append(pdata)

            if not viableposts:
                await ctx.send(embed=error_gen(ctx, self.bot.i18n.localize('reddit_no_posts', ctx=ctx)))
                return

            pdata = random.choice(viableposts)
            embed = embed_author_template(ctx, True)
            embed.title = pdata['title']
            embed.description = self.bot.i18n.localize('reddit_byline', ctx=ctx).format(pdata['author'], pdata['subreddit'], 'https://reddit.com'+pdata['permalink'])
            embed.set_image(url=pdata['url'])
            await ctx.send(embed=embed)


    @commands.command(name='count')
    @commands.is_owner()
    async def cmd_count(self, ctx: commands.Context, user: typing.Union[discord.User, int], *, regex: str):
        """Counts messages containing regex from a user."""
        async with ctx.typing():
            if not isinstance(user, int):
                uid = user.id
            else:
                uid = user
            re_search = re.compile(regex, re.IGNORECASE)
            if isinstance(ctx.channel, discord.DMChannel):
                chans: List[discord.DMChannel] = [ctx.channel]
            else:
                def canview(channel: discord.TextChannel, member: discord.Member):
                    perms = channel.permissions_for(member)
                    return perms.read_messages and perms.read_message_history
                chans: List[discord.TextChannel] = [x for x in ctx.guild.text_channels if canview(x, ctx.guild.me)]
            count = 0
            for chan in chans:
                async for msg in chan.history(limit=None):
                    if msg.author.id != uid:
                        continue
                    count += re_search.findall(msg.content)
            await ctx.send(self.bot.i18n.localize('count_cmd', ctx=ctx).format(count, user, regex, len(chans)))

    @commands.command(aliases=['add', 'join'])
    @commands.cooldown(rate=1, per=5, type=commands.BucketType.channel)
    async def invite(self, ctx):
        """Add me to your server!"""
        perms = discord.Permissions()
        perms.manage_roles = True
        perms.view_channel = True
        perms.send_messages = True
        perms.embed_links = True
        oauth_url = discord.utils.oauth_url(str(ctx.bot.user.id), perms)
        await ctx.send(self.bot.i18n.localize('invite', ctx=ctx).format(oauth_url))

    @commands.command()
    async def source(self, ctx, *, command: str = None):
        """Displays the source for a command."""
        if command is None:
            return await ctx.send(CONSTANTS['source_code'])

        obj = ctx.bot.get_command(command.replace('.', ' '))
        if obj is None:
            return await ctx.send(self.bot.i18n.localize('source_missing', ctx=ctx))

        # since we found the command we're looking for, presumably anyway, let's
        # try to access the code itself
        src = obj.callback.__code__
        lines, firstlineno = inspect.getsourcelines(src)
        if not obj.callback.__module__.startswith('discord'):
            # not a built-in command
            location = os.path.relpath(src.co_filename).replace('\\', '/')
            source_url = CONSTANTS['source_code_branch']
        else:
            location = obj.callback.__module__.replace('.', '/') + '.py'
            source_url = 'https://github.com/Rapptz/discord.py/blob/rewrite'

        final_url = f'<{source_url}/{location}#L{firstlineno}-{firstlineno + len(lines) - 1}>'
        await ctx.send(final_url)


def setup(bot):
    bot.add_cog(Miscellaneous(bot))
