import datetime

from cogs.utils.bot import Semicolon
from cogs.utils.utils import *


class Moderation(commands.Cog):
    def __init__(self, bot: Semicolon):
        self.bot = bot
        self.disabled_cmds = data_path('disabled_commands.json')
        if not os.path.exists(self.disabled_cmds):
            with open(self.disabled_cmds, 'w') as f:
                f.write('{}')

    async def channel_setup(self, ctx: commands.Context, constants_key: str):
        cname = self.bot.config[constants_key]
        existing_channel = discord.utils.get(ctx.guild.text_channels, name=cname)
        if existing_channel:
            await ctx.send(self.bot.i18n.localize('channel_fail', ctx=ctx).format(existing_channel))
            return
        create_in = ctx.guild.get_channel(ctx.channel.category_id) if ctx.channel.category_id else ctx.guild
        overwrites = {
            ctx.guild.default_role: discord.PermissionOverwrite(send_messages=False),
            ctx.guild.me: discord.PermissionOverwrite(send_messages=True, read_messages=True)
        }
        new_chan = await create_in.create_text_channel(cname, topic=self.bot.i18n.localize(constants_key + '_topic', guild=ctx.guild),
                                                       overwrites=overwrites,
                                                       reason=self.bot.i18n.localize('reason', guild=ctx.guild).format(ctx.author))
        await ctx.send(self.bot.i18n.localize('channel_created', ctx=ctx).format(new_chan))

    @commands.group(invoke_without_command=True)
    @commands.guild_only()
    async def setup(self, ctx: commands.Context):
        """Sets up a semicolon feature on a server."""
        await ctx.send(self.bot.i18n.localize('setup_default', ctx=ctx).format(ctx.prefix, ctx.invoked_with))

    @setup.command(name='joinlog', brief='Creates a channel that logs joins, leaves, & name changes.')
    @commands.has_guild_permissions(manage_channels=True)
    @commands.bot_has_guild_permissions(manage_channels=True)
    @commands.guild_only()
    async def setup_joinlog(self, ctx: commands.Context):
        """Creates a channel that logs member joins, leaves, bans, unbans, name changes, and discriminator changes."""
        await self.channel_setup(ctx, 'join_channel')

    @setup.command(name='messagelog')
    @commands.has_guild_permissions(manage_channels=True)
    @commands.bot_has_guild_permissions(manage_channels=True)
    @commands.guild_only()
    async def setup_messages(self, ctx: commands.Context):
        """Creates a channel that logs edited and deleted messages."""
        await self.channel_setup(ctx, 'message_channel')

    @setup.command(name='pronouns')
    @commands.has_guild_permissions(manage_roles=True)
    @commands.bot_has_guild_permissions(manage_roles=True)
    async def setup_pronouns(self, ctx: commands.Context):
        """Sets up automatic, cross-server pronoun roles."""
        toprole = ctx.guild.me.top_role
        for pronoun in CONSTANTS['pronoun_whitelist']:
            p_name = self.bot.config['pronoun_prefix'] + pronoun
            p_role = discord.utils.find(lambda r: r.name == p_name and toprole > r, ctx.guild.roles)
            if not p_role:
                await ctx.guild.create_role(name=self.bot.config['pronoun_prefix'] + pronoun)
                # pronoun cog will detect this role creation and create the rest. so, to avoid race conditions,
                break
        await asyncio.sleep(.1)
        await ctx.send(self.bot.i18n.localize('pronouns_created', ctx=ctx))

    @commands.command()
    @commands.has_permissions(manage_guild=True)
    @commands.guild_only()
    async def toggle(self, ctx: commands.Context, command: str, action: bool):
        """
        Enables or disables a command for the current guild.
        """
        cmd = ctx.bot.get_command(command)

        if cmd is None:
            await ctx.send(self.bot.i18n.localize('toggle_no_command', ctx=ctx))
            return

        while cmd.parent is not None:  # get the base command
            cmd = cmd.parent

        if cmd.name in ['toggle']:
            await ctx.send(self.bot.i18n.localize('toggle_no_toggle_toggle', ctx=ctx))
            return

        with open(self.disabled_cmds) as j:
            data = json.load(j)
        json_key = str(ctx.guild.id)  # json can't save ints as keys
        if json_key not in data:
            data[json_key] = []
        gdata = data[json_key]

        langkey = ["toggle"]

        if not action:
            tkey = 'failure'
            if cmd.name not in gdata:
                gdata.append(cmd.name)
                tkey = 'success'

            langkey += [tkey, 'disable']
        else:
            tkey = 'failure'
            while cmd.name in gdata:  # just incase it duped?? idk
                gdata.remove(cmd.name)
                tkey = 'success'
            if len(gdata) == 0:
                del data[json_key]

            langkey += [tkey, 'enable']

        with open(self.disabled_cmds, 'w') as j:
            json.dump(data, j)

        await ctx.send(self.bot.i18n.localize('_'.join(langkey), ctx=ctx).format(ctx.prefix, cmd.name, ctx.guild.name))

    @commands.group(aliases=['purge', 'delete'], invoke_without_command=True)
    @commands.has_permissions(manage_messages=True)
    @commands.bot_has_permissions(manage_messages=True)
    async def clear(self, ctx, messages: int):
        """
        Deletes X amount of messages.
        X does not include the message invoking the command.
        """
        warning_limit = CONSTANTS['clear_count_warning_threshold']

        if messages <= 0:
            await ctx.send(embed=error_gen(ctx, self.bot.i18n.localize('clear_count_too_low', ctx=ctx)))

        messages = messages+1
        if messages <= warning_limit or await get_yn_input(ctx, self.bot.i18n.localize('clear_large_count_warning', ctx=ctx).format(warning_limit)):
            await ctx.channel.purge(limit=messages)

    @clear.command(name='after')
    @commands.has_permissions(manage_messages=True)
    @commands.bot_has_permissions(manage_messages=True)
    async def clear_after(self, ctx, message_id: int):
        """
        Clears messages sent after a provided message ID.
        Command works by extracting the time from the ID and deleting anything afterwards, so any ID is valid input.
        Will ask for confirmation if you try to clear over 1 days worth of messages.
        """
        dtstop = discord.utils.snowflake_time(message_id)  # gets datetime object corresponding to the input

        async def the_purge():
            await ctx.channel.purge(after=dtstop, limit=None)

        if ((datetime.datetime.now() - dtstop) <= datetime.timedelta(days=1)) or await get_yn_input(ctx, self.bot.i18n.localize('clear_after_warning', ctx=ctx)):
            await the_purge()


def setup(bot):
    bot.add_cog(Moderation(bot))
