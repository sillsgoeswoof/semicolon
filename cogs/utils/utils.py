import asyncio
import math
import os
import discord
from discord.ext import commands
import typing
import json
from cogs.utils.vars import *
from discord.utils import escape_markdown as esc_md


class JsonManager:
    def __init__(self, filename):
        self.data = {}
        self._file = os.path.join(CONSTANTS['data_folder'], filename)
        os.makedirs(CONSTANTS['data_folder'], exist_ok=True)
        if not os.path.exists(self._file):
            self.save()
        self._load()

    def _load(self):
        self.data = json_load(self._file)

    def save(self):
        json_save(self._file, self.data)

    def init_data(self, key: str, init_with=None):
        """Creates objects in json dict if non-existent"""
        if init_with is None:
            init_with = {}
        if key not in self.data:
            self.data[key] = init_with


class Localizer:
    def __init__(self):
        self.config = {}
        self._config_file = os.path.join(CONSTANTS['data_folder'], "lang_select.json")
        os.makedirs(CONSTANTS['data_folder'], exist_ok=True)
        if not os.path.exists(self._config_file):
            self.save_config()
        self._load_config()

        self.langs = {}
        self.keys = []
        self.load_langs()
        self._default_lang_key = 'en_us'
        if self._default_lang_key not in self.langs:
            raise FileNotFoundError("Default language file not found! All messages will be missing!")

    def clear_removed_langs(self):
        for snowflake, lang in self.config.copy().items():
            if lang not in self.langs:
                del self.config[snowflake]
        self.save_config()

    def load_langs(self):
        path = CONSTANTS['lang_folder']
        langs = []
        for file in os.listdir(path):
            if file.startswith('_'):
                continue
            lname = file.split('.')[0]
            langs.append(lname)
            self.langs[lname] = json_load(os.path.join(path, file))
        # if a language is for some reason removed, remove it !
        for lname in self.langs.copy().keys():  # idk if i need to copy?? but just in case??
            if lname not in langs:
                del self.langs[lname]
        self.clear_removed_langs()

        # create cache of keys, for.. various purposes?
        self.keys = []
        for d in self.langs.values():
            for key in d.keys():
                # if key has not been added and is not a comment
                if key not in self.keys and not key.startswith('_'):
                    self.keys.append(key)

    def localize(self, key: str, lang: str = None, ctx: commands.context = None, user: int = None, channel: int = None,
                 guild: int = None) -> str:

        if lang is not None:
            text = self._get_text(key, lang)
            if text is not None:
                return text

        if ctx:
            if ctx.author and user is None:
                user = ctx.author.id
            if ctx.channel and channel is None:
                channel = ctx.channel.id
            if ctx.guild and guild is None:
                guild = ctx.guild.id

        if user is not None:
            text = self._get_text_by_id(key, user)
            if text is not None:
                return text
        if channel is not None:
            text = self._get_text_by_id(key, channel, is_channel=True)
            if text is not None:
                return text
        if guild is not None:
            text = self._get_text_by_id(key, guild, is_guild=True)
            if text is not None:
                return text

        # all else has failed, resort to default string
        text = self._get_text(key, self._default_lang_key)
        if text is not None:
            return text

        # PANIC!! text not found anywhere!!
        return f"[{key}]"

    def localize_p(self, key: str, count: int, lang: str = None, ctx: commands.context = None, user: int = None,
                   channel: int = None, guild: int = None) -> str:

        if count != 1:
            key += '_plural'
        return self.localize(key, lang=lang, ctx=ctx, user=user, channel=channel, guild=guild)

    def _get_text_by_id(self, key: str, snowflake: int, is_channel: bool = False, is_guild: bool = False) -> typing.Union[str, None]:
        lang = self.get_config(snowflake, is_channel=is_channel, is_guild=is_guild)
        if lang is not None:
            return self._get_text(key, lang)
        return None

    def _get_text(self, key: str, lang: str) -> typing.Union[str, None]:
        if lang in self.langs:
            ldict = self.langs[lang]
            if key in ldict:
                return ldict[key]
        return None

    def _load_config(self):
        self.config = json_load(self._config_file)

    def save_config(self):
        json_save(self._config_file, self.config)

    def _get_config_key(self, snowflake: int, is_channel: bool = False, is_guild: bool = False) -> str:
        key = str(snowflake)
        # old channels and guild can have same ID, so ya gotta differentiate
        if is_channel and is_guild:
            raise Exception("Invalid config key")
        if is_channel:
            key += 'c'
        if is_guild:
            key += 'g'
        return key

    def write_config(self, lang: str, snowflake: int, is_channel: bool = False, is_guild: bool = False):
        if lang not in list(self.langs.keys())+['_']:
            raise FileNotFoundError("Language doesn't exist")
        key = self._get_config_key(snowflake, is_channel, is_guild)
        if lang == '_':
            if key in self.config:
                del self.config[key]
        else:
            self.config[key] = lang
        self.save_config()

    def get_config(self, snowflake: int, is_channel: bool = False, is_guild: bool = False) -> typing.Union[str, None]:
        key = self._get_config_key(snowflake, is_channel, is_guild)
        if key in self.config:
            return self.config[key]
        return None


def json_load(filename: str):
    """Loads JSON file"""
    with open(filename, 'r') as f:
        return json.load(f)


def json_save(filename: str, cache: dict):
    """Saves a dict to minified json file"""
    with open(filename, 'w') as f:
        json.dump(cache, f, separators=(',', ':'))


class GuildExclusiveCommand(commands.CheckFailure):
    pass


class ConnectionFailure(Exception):
    pass


def line_split(input_message, char_limit=2000):
    output = []
    for line in input_message.split('\n'):
        line = line.strip() + '\n'
        if output and len((output[-1] + line).strip()) <= char_limit:
            output[-1] += line
        else:
            while line:
                output.append(line[:char_limit])
                line = line[char_limit:]
    return [msg.strip() for msg in output]


def guild_check(ctx: commands.Context, guild_id: int, no_name: bool = False) -> bool:
    """
    Checks if the context matches an input guild ID, and raises a descriptive error if not.
    :param ctx: context from a command
    :param guild_id: guild id to check against
    :param no_name: (optional) hides the expected server name if True
    :return: True or raises a GuildExclusiveCommand error
    """
    if ctx.guild and ctx.guild.id == guild_id:
        return True
    if no_name:
        guild = ctx.bot.i18n.localize('secret_guild', ctx=ctx)
    else:
        guild = ctx.bot.get_guild(guild_id)
        guild = guild_id if guild is None else guild
    raise GuildExclusiveCommand(ctx.bot.i18n.localize('guild_exclusive', ctx=ctx).format(guild))


def get_constant_emoji(emoji: str, ctx: commands.Context):
    # TODO: use more (find instances of use_external_emojis)
    if emoji not in CONSTANTS['emojis']:
        raise ValueError(ctx.bot.i18n.localize("emoji_not_found").format(emoji))
    emojis = CONSTANTS['emojis'][emoji]
    if isinstance(emojis, str):
        return emojis
    if not ctx.guild:
        return emojis[0]
    return emojis[not ctx.guild.me.permissions_in(ctx.channel).use_external_emojis]


async def is_chip(ctx: commands.Context) -> bool:
    """Returns bool of if current guild is ChiP or not. Hides the name because it's funny."""
    return guild_check(ctx, 381941901462470658, no_name=True)


def embed_author_template(ctx: commands.Context, subtle_author: typing.Union[bool, None] = False) -> discord.Embed:
    """Creates an embed template"""
    embed = discord.Embed(timestamp=ctx.message.created_at, color=CONSTANTS['colors']['default'])
    if subtle_author is not None:
        if not subtle_author:
            if isinstance(ctx.channel, discord.TextChannel):
                embed.set_footer(text='#' + ctx.channel.name)
            embed.set_author(name=ctx.author.name, icon_url=ctx.author.avatar_url_as(format='png', size=128))
        else:
            embed.set_footer(text=ctx.bot.i18n.localize("utils_embed_request", ctx=ctx).format(ctx.author))
    return embed


def ping_user(ctx: typing.Union[commands.Context, discord.Message, discord.abc.Snowflake]) -> discord.AllowedMentions:
    """Returns an AllowedMentions instance of the author or snowflake-type"""
    # TODO: process raw IDs somehow ?
    # TODO: process multiple inputs
    if isinstance(ctx, discord.ext.commands.Context) or isinstance(ctx, discord.Message):
        user = ctx.author
    elif isinstance(ctx, discord.abc.Snowflake):
        user = ctx
    else:
        raise ValueError("Unknown input type")  # no easy i18n method
    return discord.AllowedMentions(everyone=False, users=[user], roles=False)


async def private_message(ctx: commands.Context, msg_to_send: str):
    """Privately messages command output"""
    if isinstance(ctx.channel, discord.DMChannel):
        await ctx.send(msg_to_send)
        return
    try:
        await ctx.author.send(msg_to_send)
        perms = ctx.channel.permissions_for(ctx.guild.me)
        emoji = CONSTANTS['emojis']['DM_SENT']
        if perms.add_reactions and perms.read_message_history:
            await ctx.message.add_reaction(emoji)
        else:
            await ctx.send(ctx.bot.i18n.localize("utils_dm", ctx=ctx).format(emoji), delete_after=7)
    except discord.Forbidden:
        await ctx.send(ctx.bot.i18n.localize("utils_dm_fail", ctx=ctx).format(ctx.author.mention), delete_after=15)


def comma_separator(lst: list, oxford_comma: bool = True) -> str:
    """Creates a comma separated list, ie 'Joe, Bob, and Frank'"""
    length = len(lst)
    if length > 1:
        output_str = ', '.join(lst[:-1])
        comma = ',' if length > 2 and oxford_comma else ''
        output_str = output_str + comma + ' & ' + lst[length - 1]
    else:
        output_str = ''.join(lst)
    return output_str


def clamp(n: int, minimum: int, maximum: int) -> int:
    """Clamps an input n to fit in range [minimum, maximum]"""
    return max(minimum, min(n, maximum))


def unpack_args(ctx) -> tuple:
    return ctx.args[2:]  # gets direct command inputs, removing the 'self' and 'ctx'


def error_gen(ctx: commands.Context, error_message: str) -> discord.Embed:
    """Returns an embed containing the input error message and a footer noting the command requester."""
    embed = embed_author_template(ctx, subtle_author=True)
    embed.description = error_message
    embed.colour = discord.Colour.red()
    return embed


def data_path(filename: str):
    return os.path.join(CONSTANTS['data_folder'], filename)


def shorten(string: str, limit: int = 2000):
    # TODO: find some way to add i18n??
    if len(string) <= limit:
        return string
    ending = "[...]"  # if ctx is None else ctx.bot.i18n.localize('too_long', ctx=ctx)
    return string[:limit - len(ending)] + ending


async def delete_after(message: discord.Message, delay: float = CONSTANTS['self_delete']):
    # perm checks technically arent needed BUT they save on ratelimits
    if not message.guild:
        return
    if message.guild.me.permissions_in(message.channel).manage_messages:
        await message.edit(delete_after=delay)


def get_roles_by_prefix(prefix, source: typing.Union[discord.Guild, discord.Member]):
    top_role = source.me.top_role if isinstance(source, discord.Guild) else source.guild.me.top_role
    return list(filter(lambda x: x.name.startswith(prefix) and top_role > x, source.roles))


def get_guild_names(guilds):
    return comma_separator([f'`{esc_md(guild.name)}`' for guild in guilds])


async def set_role(user: discord.Member, role: discord.Role, add_role: bool):
    user_has_role = role in user.roles

    func = None
    if not user_has_role and add_role:
        func = user.add_roles
    elif user_has_role and not add_role:
        func = user.remove_roles

    if func:
        await func(role)


async def toggle_role(user: discord.Member, role: discord.Role):
    await set_role(user, role, role not in user.roles)


async def get_yn_input(ctx, prompt, timeout: float = 60.0):
    perms = ctx.channel.permissions_for(ctx.guild.me)
    author_perms = ctx.author.permissions_in(ctx.channel)
    dest = ctx if perms.send_messages else ctx.author  # send message to DMs if no chatting perms
    # TODO: there's probably a better way to write the ending dest == ctx.author
    can_react = (perms.add_reactions and perms.read_message_history and author_perms.read_message_history) or dest == ctx.author
    can_custom = perms.external_emojis or dest == ctx.author
    index = int(not can_custom)
    use_emojis = [CONSTANTS['emojis']['SUCCESS'][index], CONSTANTS['emojis']['FAILURE'][index]]

    y = ctx.bot.i18n.localize('utils_get_input_text_yes', ctx=ctx)
    n = ctx.bot.i18n.localize('utils_get_input_text_no', ctx=ctx)
    output = ctx.bot.i18n.localize('utils_get_input_text', ctx=ctx).format(prompt, y, n) if not can_react else prompt
    msg = await dest.send(output)
    if can_react:
        for r in use_emojis:
            await msg.add_reaction(r)

    def react_check(reaction, user):
        return user == ctx.message.author and str(reaction.emoji) in list(map(str, use_emojis)) and reaction.message.id == msg.id

    def msg_check(m):
        return m.author == ctx.message.author and m.content.lower() in [y, n] and m.channel == ctx.channel

    try:
        if can_react:
            userreaction, useruser = await ctx.bot.wait_for('reaction_add', timeout=timeout, check=react_check)
            confirmation = str(userreaction) == str(use_emojis[0])
        else:
            usermsg = await ctx.bot.wait_for('message', timeout=timeout, check=msg_check)
            confirmation = usermsg.content.lower() == y
            if dest == ctx and not confirmation:
                await usermsg.delete()
        if confirmation:
            return True
        else:
            await msg.delete()
            await ctx.message.delete()
    except asyncio.TimeoutError:
        await msg.delete()
        await ctx.message.delete()

    return False


async def get_input(ctx: commands.Context, title: str, prompt: str, items: list, selections: list):
    timeout = 60.0
    perms = ctx.channel.permissions_for(ctx.guild.me)
    # send message to DMs if no chatting perms
    dest = ctx if perms.send_messages and perms.add_reactions and perms.read_message_history else ctx.author
    emojis = [''.join([chr(48+x), chr(65039), chr(8419)]) for x in range(1, 10)] + \
             ["\N{KEYCAP TEN}", "\N{LEFTWARDS BLACK ARROW}", "\N{BLACK RIGHTWARDS ARROW}"]

    # make the embed
    embed = embed_author_template(ctx, subtle_author=True)
    embed.title = title
    base_desc = [prompt]

    # get the pages of language
    page = 0
    max_page = math.ceil(len(items)/10) - 1

    def get_current_page():
        return items[page*10:(page+1)*10]

    def get_formatted_page():
        return ["{}. {}".format(i, item) for i, item in enumerate(get_current_page(), start=1)]

    def edit_desc():
        tdesc = base_desc + get_formatted_page()
        embed.description = "\n".join(tdesc).strip()
        embed.set_footer(text=ctx.bot.i18n.localize("get_input_footer", ctx=ctx).format(page+1, max_page+1),
                         icon_url=ctx.author.avatar_url_as(format='png', size=128))

    def get_reacts():
        reacts = emojis[:len(get_current_page())]
        if page > 0:
            reacts.append(emojis[-2])
        if page < max_page:
            reacts.append(emojis[-1])
        return reacts

    # make the message
    edit_desc()
    msg = await dest.send(ctx.bot.i18n.localize("get_input_wait", ctx=ctx), embed=embed)
    is_dm = isinstance(msg.channel, discord.DMChannel)

    for r in get_reacts():
        await msg.add_reaction(r)

    def react_check(reaction, usr):
        return usr == ctx.message.author and str(reaction.emoji) in emojis and reaction.message.id == msg.id

    outkey = None
    try:
        while True:
            # wait for user input
            await msg.edit(content="", embed=embed)
            userreaction, useruser = await ctx.bot.wait_for('reaction_add', timeout=timeout, check=react_check)
            can_clear_react = not is_dm and perms.manage_messages
            if can_clear_react:
                await msg.remove_reaction(userreaction, useruser)
            # if page flip:
            if str(userreaction) in emojis[-2:]:
                # increment page & edit
                page = clamp(page + (-1 if str(userreaction) == emojis[-2] else 1), 0, max_page)
                edit_desc()
                await msg.edit(content=ctx.bot.i18n.localize("get_input_wait", ctx=ctx), embed=embed)

                # add/remove reactions
                msg = await msg.channel.fetch_message(msg.id)  # need to fetch message to get reacts
                expect_reacts = get_reacts()
                msg_reacts = list(map(str, msg.reactions))
                for e in emojis:
                    if e in expect_reacts and e not in msg_reacts:
                        await msg.add_reaction(e)
                    elif e in msg_reacts and e not in expect_reacts:
                        if can_clear_react:
                            await msg.clear_reaction(e)
                        else:
                            botself = msg.me if is_dm else msg.guild.me
                            await msg.remove_reaction(e, botself)
            else:  # a language has been selected
                outkey = selections[page*10 + int(str(userreaction)[0]) - 1]
                break
    except (asyncio.TimeoutError, discord.NotFound):
        pass
    finally:
        # delete messages if possible
        await msg.delete(delay=0.01)
        await ctx.message.delete(delay=0.01)
        return outkey
