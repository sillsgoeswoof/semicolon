import datetime

from discord.utils import escape_markdown

from cogs.utils.bot import Semicolon
from cogs.utils.utils import *
import asyncio

joinlog_queue = {}
joinlock = asyncio.Lock()


class Deletable:
    # own class for message delete events bc raw bulk delete doesn't work how i expected and im too lazy to recode
    __slots__ = {'channel_id', 'message_id'}

    def __init__(self, channel_id, message_id):
        self.channel_id = channel_id
        self.message_id = message_id


async def joinlog_post(channel: discord.TextChannel, message: str):
    """
    Groups together joinlog messages sent within a second of each other
    :param channel: the channel to post to
    :param message: the message to send
    """
    to_post = False  # whether or not this call of the function will be responsible for posting the final output
    cid = channel.id  # shorthand for ID of the channel

    async with joinlock:  # avoids race conditions with a lock. might want to remove for scalability later
        if cid not in joinlog_queue:
            # first post this second, will take responsibility for posting
            joinlog_queue[cid] = [message]
            to_post = True
        else:
            joinlog_queue[cid].append(message)
    if to_post:
        await asyncio.sleep(1)
        async with joinlock:
            # split into <=2000 character messages
            for msg in line_split('\n'.join(joinlog_queue[cid])):
                await channel.send(msg)
            del joinlog_queue[cid]


class Joinlog(commands.Cog):
    def __init__(self, bot: Semicolon):
        self.bot = bot

    async def joinlog_process(self, member: discord.Member, guild: discord.Guild, event: str):
        """
        Processes member joins and leaves
        :param member: discord.py member object
        :param guild: the originating guild
        :param event: the event that triggered this function. can be "JOIN", "LEAVE", "BAN", "UNBAN"
        """
        logchan = discord.utils.get(guild.text_channels, name=self.bot.config['join_channel'])
        if logchan is None:
            return

        guildperms = guild.me.guild_permissions
        chanperms = guild.me.permissions_in(logchan)
        if not (chanperms.send_messages and chanperms.view_channel):
            return

        emojiindex = int(not chanperms.use_external_emojis)
        emoji = CONSTANTS['emojis'][event][emojiindex]
        new_join = ""
        reason = ""

        i18n_p = 'joinlog_'
        output = self.bot.i18n.localize(i18n_p+'prefix', channel=logchan, guild=guild) + self.bot.i18n.localize(i18n_p+event, channel=logchan, guild=guild).strip()

        if event == "JOIN":
            if member.created_at >= (datetime.datetime.now() - datetime.timedelta(days=1)):
                new_join = CONSTANTS['emojis']['NEW_JOIN']
        elif event == "BAN":
            if guildperms.ban_members:
                ban = await guild.fetch_ban(member)
                reason = self.bot.i18n.localize(i18n_p+'reason', channel=logchan, guild=guild).format(ban.reason)

        foutput = output.format(emoji, member, guild.member_count, new_join, reason, escape_markdown(str(member)))
        await joinlog_post(logchan, foutput)

    @commands.Cog.listener()
    async def on_member_join(self, member: discord.Member):
        await self.joinlog_process(member, member.guild, "JOIN")

    @commands.Cog.listener()
    async def on_member_remove(self, member: discord.Member):
        await self.joinlog_process(member, member.guild, "LEAVE")

    @commands.Cog.listener()
    async def on_member_ban(self, guild: discord.Guild, member: discord.Member):
        await self.joinlog_process(member, guild, "BAN")

    @commands.Cog.listener()
    async def on_member_unban(self, guild: discord.Guild, member: discord.Member):
        await self.joinlog_process(member, guild, "UNBAN")

    @commands.Cog.listener()
    async def on_user_update(self, before: discord.User, after: discord.User):
        # todo: check if "if after in x.members" works
        name_change = before.name != after.name
        disc_change = before.discriminator != after.discriminator
        if not (name_change or disc_change):
            return
        guilds = [x for x in self.bot.guilds if discord.utils.get(x.members, id=after.id)]
        for g in guilds:
            logchan = discord.utils.get(g.text_channels, name=self.bot.config['join_channel'])
            if logchan is None:
                continue
            chanperms = g.me.permissions_in(logchan)
            if not (chanperms.send_messages and chanperms.view_channel):
                continue

            i18n_p = 'joinlog_'
            output = []
            if name_change:
                output.append(self.bot.i18n.localize(i18n_p+'NAME', channel=logchan, guild=logchan.guild))
            if disc_change:
                if not name_change:
                    i18n_p += 'ONLY_'
                output.append(self.bot.i18n.localize(i18n_p+'DISCRIM', channel=logchan, guild=logchan.guild))
            await joinlog_post(logchan, '\n'.join(output).format(before, after, escape_markdown(str(before)), escape_markdown(str(after))))

    @commands.Cog.listener()
    async def on_message_edit(self, before: discord.Message, after: discord.Message):
        if not after.type == discord.MessageType.default:
            return
        if after.content == before.content:
            return
        log_chan = discord.utils.get(after.guild.text_channels, name=self.bot.config['message_channel'])
        if log_chan is None:
            return
        perms = log_chan.permissions_for(log_chan.guild.me)
        if not (perms.read_messages and perms.send_messages):
            return
        if not before.content:
            before.content = self.bot.i18n.localize('message_blank_content', channel=log_chan, guild=log_chan.guild)
        if not after.content:
            after.content = self.bot.i18n.localize('message_blank_content', channel=log_chan, guild=log_chan.guild)

        startkey = 'message_edit_'
        def mlang(input): return self.bot.i18n.localize(startkey+input, channel=log_chan, guild=log_chan.guild)

        embed = discord.Embed(title=mlang('title').format(after.channel),
                              timestamp=after.created_at,
                              color=CONSTANTS['colors'][startkey[:-1]])
        embed.set_author(name=after.author.name, icon_url=after.author.avatar_url_as(format='png', size=128))
        embed.description = mlang('desc').format(after.jump_url)
        embed.add_field(name=mlang('old_title'), value=shorten(before.content, 1024), inline=False)
        embed.add_field(name=mlang('new_title'), value=shorten(after.content, 1024), inline=False)
        await log_chan.send(embed=embed)

    async def message_delete(self, message: typing.Union[discord.Message, Deletable, list]):
        # get message data
        is_message = isinstance(message, discord.Message)
        if not is_message:
            channel = self.bot.get_channel(message.channel_id)
            guild = channel.guild
            message_id = message.message_id
        else:
            guild = message.guild
            channel = message.channel
            message_id = message.id

        if not guild:  # ignore DMs or somethin
            return

        # get channel
        log_chan = discord.utils.get(guild.text_channels, name=self.bot.config['message_channel'])
        if log_chan is None:
            return
        # ensure perms
        perms = log_chan.permissions_for(log_chan.guild.me)
        if not (perms.read_messages and perms.send_messages):
            return

        # language variables
        startkey = 'message_delete_'
        def mlang(input): return self.bot.i18n.localize(startkey + input, channel=log_chan, guild=log_chan.guild)

        # embed formatting
        title_key = 'title' if is_message else 'uncached_title'
        title = self.bot.i18n.localize(startkey+title_key, channel=log_chan, guild=guild)
        embed = discord.Embed(title=title.format(channel),
                              timestamp=discord.utils.snowflake_time(message_id),
                              color=CONSTANTS['colors'][startkey[:-1]])
        if isinstance(message, discord.Message):
            embed.set_author(name=message.author.name, icon_url=message.author.avatar_url_as(format='png', size=128))
            embed.description = message.system_content  # no shortening necessary, desc limit is at least 2000

        # TODO: add channel id and maybe emojis. also add to edit logs
        embed.set_footer(text=mlang('footer').format(message_id))
        await log_chan.send(embed=embed)

    @commands.Cog.listener()
    async def on_raw_message_delete(self, payload: discord.RawMessageDeleteEvent):
        if payload.cached_message:
            send = payload.cached_message
        else:
            send = Deletable(channel_id=payload.channel_id, message_id=payload.message_id)
        await self.message_delete(send)

    @commands.Cog.listener()
    async def on_raw_bulk_message_delete(self, payload: discord.RawBulkMessageDeleteEvent):
        for mid in payload.message_ids:
            cached_msg = discord.utils.get(payload.cached_messages, id=mid)
            if cached_msg is None:
                cached_msg = Deletable(channel_id=payload.channel_id, message_id=mid)
            await self.message_delete(cached_msg)


def setup(bot):
    bot.add_cog(Joinlog(bot))
