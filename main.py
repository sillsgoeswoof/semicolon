from cogs.utils.bot import Semicolon


def main():
    bot = Semicolon()
    bot.run()


if __name__ == '__main__':
    main()
