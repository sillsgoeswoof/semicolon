import datetime

import aiohttp
import discord
import asyncio
from discord.ext import commands
import logging

from .utils import *
from .vars import *
import numpy as np

from yaml import load
try:
    from yaml import CLoader as Loader
except ImportError:
    from yaml import Loader


class GuildDisabledCommand(commands.CheckFailure):
    pass


class Semicolon(commands.Bot):
    def __init__(self, *args, **kwargs):
        self.config = load(open('config.yaml', 'r'), Loader)
        self.i18n = Localizer()

        self.session = aiohttp.ClientSession()

        os.makedirs(CONSTANTS['data_folder'], exist_ok=True)
        logging.basicConfig(level=logging.WARN, format='[%(name)s %(levelname)s] %(message)s')
        self.logger = logging.getLogger('bot')

        intents = discord.Intents.default()
        intents.typing = False  # spammy & unneeded
        intents.integrations = False  # unneeded
        intents.webhooks = False  # unneeded
        intents.invites = False  # unneeded
        intents.voice_states = False  # unneeded

        # presences rationale: used in test.py but can be sacrificed
        intents.presences = True

        # members rationale: needed for pronouns (join/leave, role cache), joinlogs (join/leave, name change), etc
        intents.members = True

        super().__init__(
            command_prefix=discord.ext.commands.when_mentioned_or(self.config['prefix']),
            description=CONSTANTS['description'],
            case_insensitive=True,
            activity=discord.Game(CONSTANTS['game'].format(self.config['prefix'])),
            allowed_mentions=discord.AllowedMentions(everyone=False, users=False, roles=False),
            intents=intents,
            member_cache_flags=discord.MemberCacheFlags.from_intents(intents),
            *args,
            **kwargs
        )

        self.add_check(self.blocked_guild)

    async def on_ready(self):
        # overly complicated fancy formatting
        def get_str_length(number):
            number = len(str(number))
            return np.math.ceil(number / 3) - 1 + number

        big_numbers = tuple(map(len, [self.guilds, list(self.get_all_channels()), self.users]))
        guilds, channels, users = big_numbers
        pad_to = max(map(get_str_length, big_numbers))
        pad_str = "{:>" + str(pad_to) + ",}"
        print(self.i18n.localize("login_user").format(self.user))
        print(self.i18n.localize("login_guilds").format(pad_str.format(guilds)))
        print(self.i18n.localize("login_channels").format(pad_str.format(channels)))
        print(self.i18n.localize("login_users").format(pad_str.format(users)))

    async def on_command_error(self, ctx: commands.Context, error: Exception):
        passable_errors = commands.ConversionError, commands.UserInputError, commands.CheckFailure, \
                          commands.DisabledCommand, commands.CommandOnCooldown, commands.MaxConcurrencyReached, \
                          GuildDisabledCommand
        delete = 10

        if isinstance(error, commands.CommandNotFound) or \
                (any([isinstance(error, x) for x in passable_errors]) and ctx.command.name == '-;'):
            pass
        elif isinstance(error, commands.CommandOnCooldown) and ctx.command.name == "catch":
            embed = embed_author_template(ctx, subtle_author=True)
            embed.description = self.i18n.localize('pokemon_ratelimit', ctx=ctx)
            embed.colour = discord.Colour.greyple()
            await ctx.send(embed=embed)
        elif isinstance(error, commands.CommandOnCooldown):
            stopwait = ctx.message.created_at + datetime.timedelta(seconds=error.retry_after)
            chn = ctx.channel
            botself = ctx.guild.me if isinstance(chn, discord.TextChannel) else chn.me
            perms = chn.permissions_for(botself)
            if perms.add_reactions and perms.read_message_history:
                react_index = CONSTANTS['emojis']['RATELIMIT'][int(not perms.use_external_emojis)]
                await ctx.message.add_reaction(react_index)
                thereact = None
                while not thereact:
                    thereact = list(filter(lambda x: str(x) == react_index, ctx.message.reactions))
                    if not thereact:  # avoid sleeping if unnecessary
                        await asyncio.sleep(1)
                await discord.utils.sleep_until(stopwait)
                try:
                    await thereact[0].remove(botself)
                except discord.errors.NotFound:
                    pass
        elif isinstance(error, GuildDisabledCommand):
            await ctx.send(self.i18n.localize('error_guild_disabled', ctx=ctx).format(ctx.guild.name), delete_after=delete)
        elif isinstance(error, commands.NotOwner):
            # await ctx.send("You cannot access that command.")
            pass
        elif isinstance(error, commands.CommandInvokeError):
            cmention = f"in {ctx.channel.mention}" if isinstance(ctx.channel, discord.TextChannel) else 'in DMs'
            ename = str(type(error).__name__)
            emsg = self.i18n.localize('error_dm').format(ename, cmention, ctx.author.mention, ctx.message.content, error)
            await (await self.application_info()).owner.send(emsg)
        else:
            await ctx.send(embed=error_gen(ctx, str(error)))

    async def blocked_guild(self, ctx):
        """
        Checks if the guild has blocked this command
        """
        if not ctx.guild:
            return True
        if ctx.author.guild_permissions.administrator or ctx.author == ctx.guild.owner:
            return True

        # TODO: should find a way to cache this, i don't imagine this is super efficient?
        filepath = data_path('disabled_commands.json')
        if os.path.exists(filepath):
            with open(filepath) as j:
                data = json.load(j)
            json_key = str(ctx.guild.id)
            if json_key in data:  # if guild is in data
                cmd = ctx.command
                while cmd.parent is not None:  # get the base command
                    cmd = cmd.parent

                if cmd.name in data[json_key]:  # if cmd name is in disabled list
                    raise GuildDisabledCommand
        return True

    async def process_commands_sudo(self, message):
        # From https://gitlab.com/HTSTEM/DiscordBot/-/blob/python-ext.com/cogs/util/bot.py, MIT license
        ctx = await self.get_context(message)
        await self.invoke_sudo(ctx)

    async def invoke_sudo(self, ctx):
        # From https://gitlab.com/HTSTEM/DiscordBot/-/blob/python-ext.com/cogs/util/bot.py, MIT license
        if ctx.command is not None:
            self.dispatch('command', ctx)
            try:
                if 'no_sudo' not in [i.__qualname__.split('.')[0] for i in ctx.command.checks]:
                    if isinstance(ctx.command, commands.Group):
                        subcommand = ctx.message.content.replace(ctx.prefix, '', 1)
                        subcommand = ' '.join(subcommand.split(' ')[1:])
                        command = ctx.command.get_command(subcommand)
                        ctx.command = command

                    async def nc(ctx_):
                        return True

                    checks = ctx.command.can_run
                    ctx.command.can_run = nc
                    try:
                        await ctx.command.invoke(ctx)
                    except Exception as e:
                        await ctx.command.dispatch_error(ctx, e)
                    finally:
                        ctx.command.can_run = checks

            except commands.CommandError as e:
                await ctx.command.dispatch_error(ctx, e)
            else:
                self.dispatch('command_completion', ctx)
        elif ctx.invoked_with:
            exc = commands.CommandNotFound(self.i18n.localize("command_not_found", ctx=ctx).format(ctx.invoked_with))
            self.dispatch('command_error', ctx, exc)

    def run(self):
        token = self.config['token']
        del self.config['token']

        cog_path = CONSTANTS['cog_folder']
        for cog in ['.'.join([cog_path, f.split('.')[0]]) for f in os.listdir(cog_path) if f.endswith('.py') and not f.startswith('_') and f.split('.')[0] not in self.config['cog_blacklist']]:
            try:
                self.load_extension(cog)
            except Exception as e:
                self.logger.exception(self.i18n.localize("cog_load_fail").format(cog))
            else:
                self.logger.info(self.i18n.localize("cog_load").format(cog))

        super().run(token)
